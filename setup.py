from setuptools import setup

from pileuppy import __version__

with open('README.md') as inp:
    long_description = inp.read()

with open('requirements.txt') as inp:
    requirements = list(map(str.strip, inp))

setup(name='pileuppy',
    version=__version__,
    description='Colorful and fast tool designed to draw alignment pileup.',
    long_description=long_description,
    author='Timofey Prodanov',
    packages=['pileuppy'],
    package_dir={'pileuppy': 'pileuppy'},
    entry_points={
        "console_scripts": ['pileuppy = pileuppy.pileuppy:main']
    },
    install_requires=requirements,
    url='https://gitlab.com/tprodanov/pileuppy',
    )
