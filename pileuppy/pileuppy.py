#!/usr/bin/env python3

import sys
import os.path
import collections
import operator
import string
import re

import argparse
import pysam
try:
    import colored
except ImportError:
    pass

from pileuppy.region import Region
from pileuppy.colors import Colors, Text
from pileuppy import __version__

__author__ = 'Timofey Prodanov, Vikas Bansal'
__credits__ = __author__.split(', ')
__license__ = 'MIT'
__maintainer__ = __credits__[0]

MIN_READS = 4
SMALL_MAPQ = 10
SMALL_BQ = 13
SYMBOLS = string.ascii_letters + string.digits + string.punctuation


def panic(msg):
    sys.stderr.write(str(Text('ERROR: ', 'colored' in sys.modules).fg('red')))
    sys.stderr.write(str(msg) + '\n')
    exit(1)


def warn(msg):
    sys.stderr.write(str(Text('WARN: ', 'colored' in sys.modules).fg('#ff9966')))
    sys.stderr.write(str(msg) + '\n')


def shorthand(index):
    return SYMBOLS[min(index, len(SYMBOLS) - 1)]


def write_legend(outp, args, colors):
    prefix = args.header_prefix
    outp.write('%s%s:\n' % (prefix, colors.text('Legend').attr('bold')))

    if args.min_mapq < SMALL_MAPQ:
        outp.write('%s    %s with mapping quality less than %d\n'
            % (prefix, colors.text('Reads').fg(colors.low_mapq), SMALL_MAPQ))

    if args.filt_flags != 1796:
        outp.write('%s    %s with flag matching 1796 (UNMAP,SECONDARY,QCFAIL,DUP)\n'
            % (prefix, colors.text('Reads').attr('underlined')))
    outp.write('%s    %s with flag matching 2048 (SUPPLEMENTARY)\n'
        % (prefix, colors.text('Reads').bg(colors.supplementary)))

    outp.write('%s    %s less than %d\n'
        % (prefix, colors.text('Base qualities').fg(colors.low_bq[0]), SMALL_BQ))
    if args.min_bq:
        outp.write('%s    %s with base quality less than %d\n'
            % (prefix, colors.text('Nucleotides').fg(colors.low_bq[1]), args.min_bq))
    if not args.show_names:
        outp.write('%s    Digits on top of pileup: first digit of MAPQ (9 if MAPQ > 99)\n' % prefix)

    outp.write('%s    Using nucleotide color scheme: %s\n' % (prefix, 'logo' if args.use_logo else 'no logo'))
    outp.write('%s        %s %s '
        % (prefix, colors.color_insertion_sign(colors.text('+')), colors.color_deletion(colors.text('-'))))
    if args.use_logo:
        outp.write(' '.join(str(colors.color_base(colors.text(nt), 'X', None, None)) for nt in 'ACGT'))
    else:
        outp.write(str(colors.color_base(colors.text('X'), 'Y', None, None)))
    outp.write('\n%s    Soft clipping: %s\n' % (prefix, colors.text('^ $').fg(colors.clipping)))
    outp.write('%s\n' % prefix)


class ReadInfo:
    def __init__(self, bam_file_ix, read, sample, full_info):
        self.bam_file_ix = bam_file_ix
        self.start = read.reference_start
        self.end = read.reference_end
        self.mapq = read.mapping_quality
        self.flag = read.flag
        self.sample = sample

        if full_info:
            self.name = read.query_name
            self.tags = read.get_tags()
            self.cigar = read.cigarstring


def get_read_groups(bam_file):
    """
    Returns list of pairs (group_id, sample).
    """
    read_groups = []
    for line in str(bam_file.header).splitlines():
        if line.startswith('@RG'):
            has_rg = True

            id_m = re.search(r'ID:([ -~]+)', line)
            sample_m = re.search(r'SM:([ -~]+)', line)
            if id_m is None or sample_m is None:
                panic('Cannot find ID or SM field in the header line: "{}"'.format(line))
            read_groups.append((id_m.group(1), sample_m.group(1)))
    return read_groups


def sample_matches(sample, allowed_samples):
    if not allowed_samples:
        return True
    if not sample:
        return False

    for pattern in allowed_samples:
        m = re.match(pattern, sample)
        if m is not None and m.span()[1] == len(sample):
            return True
    return False


def extend_reads(seq_matrix, qual_matrix, read_info, bam_file_ix, bam_file, region, display_region, args):
    """
    *_matrix: matrix of sequences/qualities. Size: n_reads x len(display_region).
        If out of bounds for a read, element contains None.
        If deletion, contains nt '-' and qual None.
        If insertion, contains <long seq> and <single qual>.
    read_info: list of ReadInfo, same order as rows in the matrix.
    """
    start = display_region.start

    read_groups_list = get_read_groups(bam_file)
    if args.samples:
        read_groups = {}
        for group, sample in read_groups_list:
            if sample_matches(sample, args.samples):
                read_groups[group] = sample
        # No matching read groups found.
        if read_groups_list and not read_groups:
            return
    else:
        read_groups = dict(read_groups_list)

    for read in bam_file.fetch(region.chrom, region.start, region.end):
        if read.mapping_quality < args.min_mapq or (args.req_flags and (read.flag & args.req_flags) != args.req_flags) \
                or (read.flag & args.filt_flags) or read.is_unmapped:
            continue
        sample = read_groups.get(read.get_tag('RG')) if read.has_tag('RG') else None
        if args.samples and not sample:
            continue

        info = ReadInfo(bam_file_ix, read, sample, full_info=args.show_names)
        aln_start = read.query_alignment_start
        aln_end = read.query_alignment_end
        ref_start = info.start
        ref_end = info.end

        prev_ref_pos = None
        rev_strand = read.is_reverse
        read_seq = read.query_sequence
        if read_seq is None:
            warn("Read {} has no sequence, skipping".format(read.query_name))
            continue
        read_qual = read.query_qualities
        seq_row = [None] * len(display_region)
        qual_row = [None] * len(display_region)
        read_info.append(info)

        for query_pos, ref_pos in read.get_aligned_pairs():
            if query_pos is not None and (query_pos < aln_start or query_pos >= aln_end):
                continue

            if ref_pos is None:
                if prev_ref_pos is None:
                    continue
                ref_pos = prev_ref_pos
            else:
                prev_ref_pos = ref_pos
            if ref_pos < display_region.start:
                continue
            if ref_pos >= display_region.end:
                break

            if query_pos is None:
                seq_row[ref_pos - start] = '-'
                qual_row[ref_pos - start] = None
                continue

            nt = read_seq[query_pos] if read_seq else '?'
            if rev_strand:
                nt = nt.lower()
            qual = read_qual[query_pos] if read_qual else None

            # Insertion
            if seq_row[ref_pos - start]:
                seq_row[ref_pos - start] += nt
            else:
                seq_row[ref_pos - start] = nt
                qual_row[ref_pos - start] = qual

        if aln_start and ref_start - 1 >= display_region.start:
            seq_row[ref_start - 1 - start] = '^'
        if aln_end < len(read_seq) and ref_end < display_region.end:
            seq_row[ref_end - start] = '$'

        seq_matrix.append(seq_row)
        qual_matrix.append(qual_row)


class Batches:
    def __init__(self, filenames, read_info, join_samples):
        self.region = None
        self.display_region = None

        indices = sorted(range(len(read_info)), key=lambda i: (read_info[i].start, read_info[i].end))
        batch_indices = collections.defaultdict(list)
        for i in indices:
            key = (read_info[i].bam_file_ix, None if join_samples else read_info[i].sample)
            batch_indices[key].append(i)

        self.read_info = read_info
        self.filenames = []
        self.names = []
        self.read_indices = []
        for bam_file_ix, sample in sorted(batch_indices):
            filename = filenames[bam_file_ix]
            if '=' in filename:
                name, filename = filename.split('=', 1)
            else:
                name = os.path.basename(filename)
                ext = name.rsplit('.', 1)[-1]
                if ext == 'bam' or ext == 'cram':
                    name = name[: -len(ext) - 1]

            if sample and sample != name:
                name += ': %s' % sample
            self.filenames.append(filename)
            self.names.append(name)
            self.read_indices.append(batch_indices[(bam_file_ix, sample)])

    def set_regions(self, region, display_region):
        self.region = region
        self.display_region = display_region

    def write_read_info(self, prefix, colors, outp):
        for filename, batch_name, batch_indices in zip(self.filenames, self.names, self.read_indices):
            outp.write('%sAlignment file %s (%s):\n' % (prefix, colors.text(batch_name).attr('bold'), filename))
            name_len = max(len(self.read_info[i].name) for i in batch_indices)
            fmt_line = prefix + '    {shorthand}: {name:<%d}    %s:{start:}-{end:}, ' \
                % (name_len, self.display_region.chrom) + \
                'mapQ: {mapq}, flag: {flag}, {sample}tags: {tags}, cigar: {cigar}\n'

            for i, j in enumerate(batch_indices):
                read = self.read_info[j]
                flag = colors.text('%4d' % read.flag) \
                    .attr_if(read.flag & 1796, 'underlined') \
                    .bg_if(read.flag & 2048, colors.supplementary)
                mapq = colors.text('%3d' % read.mapq) \
                    .fg_if(read.mapq < SMALL_MAPQ, colors.low_mapq)

                outp.write(fmt_line.format(shorthand=colors.text(shorthand(i)).attr('bold'),
                    name=read.name, start=read.start + 1, end=read.end, mapq=mapq, flag=flag,
                    sample='sample: %s, ' % read.sample if read.sample else '',
                    tags=', '.join(map('%s=%s'.__mod__, read.tags)), cigar=read.cigar))
            outp.write('%s\n' % prefix)

    def write_header_lines(self, args, colors, outp):
        self.pos_fmt = '%{}s  '.format(max(len(str(self.display_region.end)), 3))
        if not args.skip_chrom:
            self.pos_fmt = '%-{}s  '.format(max(len(self.display_region.chrom), 5)) + self.pos_fmt
        second_line = self.pos_fmt % ('pos' if args.skip_chrom else ('chrom', 'pos') ) + 'ref'
        first_line = ' ' * len(second_line)

        for batch_name, batch_indices in zip(self.names, self.read_indices):
            first_line += '  '
            second_line += '  '

            curr_second_line = ''
            for i, j in enumerate(batch_indices):
                read = self.read_info[j]
                name = colors.text(shorthand(i) if args.show_names else min(99, read.mapq) // 10) \
                    .attr_if(read.flag & 1796, 'underlined') \
                    .bg_if(read.flag & 2048, colors.supplementary) \
                    .fg_if(read.mapq < SMALL_MAPQ, colors.low_mapq)
                curr_second_line += str(name)
            if len(batch_indices) < MIN_READS:
                curr_second_line += ' ' * (MIN_READS - len(batch_indices))
            m = max(MIN_READS, len(batch_indices))

            if not args.skip_bq:
                curr_second_line += ' ' + curr_second_line
                m = 2 * m + 1
            second_line += 'cov %s' % curr_second_line
            m += 4
            first_line += batch_name[:m] + ' ' * (m - len(batch_name))

        outp.write('%s\n' % first_line)
        outp.write('%s  insertions\n' % second_line)

    def write_row(self, pos, ref_nt, seq_matrix, qual_matrix, args, colors, outp):
        coverages = [sum(self.read_info[i].start <= pos < self.read_info[i].end for i in batch_indices)
            for batch_indices in self.read_indices]
        if args.skip_empty and not any(coverages):
            return

        pos_text = colors.text(self.pos_fmt % (pos + 1 if args.skip_chrom else (self.region.chrom, pos + 1))) \
            .fg_if(not self.region.contains(pos), colors.out_of_region)
        outp.write(str(pos_text))
        if args.use_logo:
            outp.write(str(colors.color_logo(colors.text(ref_nt))))
        else:
            outp.write(ref_nt)
        outp.write('  ')

        pos_ix = pos - self.display_region.start
        insertions = []
        for cov, batch_indices in zip(coverages, self.read_indices):
            outp.write('  %3d ' % min(999, cov))
            qualities = ''
            curr_insertions = []
            for column, read_ix in enumerate(batch_indices):
                seq = seq_matrix[read_ix][pos_ix]
                qual = qual_matrix[read_ix][pos_ix]

                nt_text = colors.text(seq).bg_if(column % 2 and colors.column, colors.column)
                qual_text = colors.text(' ' if qual is None else chr(qual + 33)) \
                    .bg_if(column % 2 and colors.column, colors.column)
                if seq is None:
                    nt_text.text = ' '

                elif len(seq) > 1:
                    nt_text.text = '+'
                    colors.color_insertion_sign(nt_text)
                    curr_insertions.append(colors.color_insertion_seq(seq, ref_nt, qual, args.min_bq))

                elif seq == '-':
                    colors.color_deletion(nt_text)
                elif seq == '^' or seq == '$':
                    nt_text.fg(colors.clipping)
                else:
                    colors.color_base(nt_text, ref_nt, qual, args.min_bq)
                    qual_text.fg_if(qual is not None and qual < SMALL_BQ, colors.low_bq[0])

                outp.write(str(nt_text))
                qualities += str(qual_text)

            if len(batch_indices) < MIN_READS:
                outp.write(' ' * (MIN_READS - len(batch_indices)))
                qualities += ' ' * (MIN_READS - len(batch_indices))
            if not args.skip_bq:
                outp.write(' ')
                outp.write(qualities)
            if curr_insertions:
                insertions.append(';'.join(curr_insertions))

        for curr_insertions in insertions:
            outp.write('  %s' % curr_insertions)
        outp.write('\n')


def prepare_colors_and_output(args):
    if args.scheme:
        args.scheme = args.scheme.lower()
    if args.scheme == 'none' or (args.scheme is None and args.output is not None):
        args.scheme == 'none'
    elif args.scheme is None:
        args.scheme = 'black'
    colors = Colors(args.scheme, args.use_logo)

    if args.output is None:
        output = sys.stdout
    else:
        output = open(args.output, 'w')

    if args.no_columns:
        colors.column = None

    if args.header:
        args.header = args.header.lower()
    if args.header == 'skip':
        args.header_prefix = None
    elif args.header == 'comment':
        args.header_prefix = '# '
    else:
        args.header_prefix = ''

    args.size = args.size.lower()
    if args.size == 'none':
        args.size = sys.maxsize
    else:
        args.size = int(args.size)

    return colors, output


def process_region(region, fasta_file, bam_files, args, colors, outp):
    if fasta_file:
        display_region = region.add_padding(args.display, fasta_file)
        ref_region = fasta_file.fetch(region.chrom, display_region.start, display_region.end)
    else:
        display_region = region.add_padding(args.display, None)
        ref_region = None

    seq_matrix = []
    qual_matrix = []
    read_info = []
    for i, bam_file in enumerate(bam_files):
        extend_reads(seq_matrix, qual_matrix, read_info, i, bam_file, region, display_region, args)

    batches = Batches(args.input, read_info, args.join_samples)
    batches.set_regions(region, display_region)
    if args.show_names and args.header_prefix is not None:
        batches.write_read_info(args.header_prefix, colors, outp)

    batches.write_header_lines(args, colors, outp)
    for pos in range(display_region.start, display_region.end):
        ref_nt = ref_region[pos - display_region.start].upper() if ref_region else 'N'
        batches.write_row(pos, ref_nt, seq_matrix, qual_matrix, args, colors, outp)


def _open_aln_file(filename, ref_filename):
    if filename.endswith('.cram') and ref_filename is None:
        panic('Fasta reference is required to work with CRAM files!')
    return pysam.AlignmentFile(filename, reference_filename=ref_filename)


def process(args):
    colors, outp = prepare_colors_and_output(args)
    if colors.use_color:
        if 'colored' in sys.modules:
            colored.set_tty_aware(awareness=False)
        else:
            panic('Trying to print colored output, but failed to import "colored" module!')

    region = Region.from_str(args.region)
    fasta_file = pysam.FastaFile(args.fasta_ref) if args.fasta_ref else None
    bam_files = []
    for i, filename in enumerate(args.input):
        if '=' in filename:
            filename = filename.split('=', 1)[1]
        bam_files.append(_open_aln_file(filename, args.fasta_ref))

    if args.header_prefix is not None:
        outp.write('%s%s\n' % (args.header_prefix, ' '.join(sys.argv)))
        if colors.use_color and not args.skip_legend:
            write_legend(outp, args, colors)

    if len(region) <= args.size:
        process_region(region, fasta_file, bam_files, args, colors, outp)

    else:
        for i, subregion in enumerate(region.split_into_smaller(args.size)):
            if i:
                outp.write('\n')
            if args.header_prefix is not None:
                outp.write('%sRegion %s\n' % (args.header_prefix, colors.text(subregion).attr('bold')))
            process_region(subregion, fasta_file, bam_files, args, colors, outp)

    if fasta_file:
        fasta_file.close()
    for bam_file in bam_files:
        bam_file.close()


def main():
    parser = argparse.ArgumentParser(
        description='Pileups reads in a specified region.',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s in1.bam [in2.bam ...] [-f ref.fa] '
            '-r chrom:start-end [-o out.pileup] [options]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('input', metavar='FILE', nargs='+',
        help='Required: Input indexed BAM/CRAM files. Allows format name=path,\n'
            'in that case name will be displayed in pileup instead of filename.')
    io_args.add_argument('-f', '--fasta-ref', metavar='FILE',
        help='Optional: Input reference indexed FASTA file.')
    io_args.add_argument('-o', '--output', metavar='FILE',
        help='Optional: Output file [stdout]. Disables colors.')

    reg_args = parser.add_argument_group('Region arguments')
    reg_args.add_argument('-r', '--region', metavar='STR', required=True,
        help='Required: pileup region in one of the following formats:\n'
            '  chrom:pos - pileup of a single position,\n'
            '  chrom:start-end - closed interval, 1-based positions,\n'
            '  chrom:pos^size - same as chrom:[pos - size]-[pos + size].')
    reg_args.add_argument('-d', '--display', metavar='INT', type=int, default=0,
        help='Display additional INT positions around the region.\n'
            'Only displays reads that cover <region>.')
    reg_args.add_argument('--size', metavar='INT|none', default='150',
        help='Split region longer than INT [%(default)s].')

    filt_args = parser.add_argument_group('Filering arguments')
    filt_args.add_argument('-q', '--min-mapq', metavar='INT', type=int, default=0,
        help='Skip alignments with mapQ less than INT [%(default)s].')
    filt_args.add_argument('-Q', '--min-bq', metavar='INT', type=int, default=0,
        help='Print ? instead of base pairs with quality less than INT [%(default)s].')
    filt_args.add_argument('--rf', '--req-flags', metavar='INT', type=int, default=0, dest='req_flags',
        help='Required flags: skip reads with mask bits unset [%(default)s].')
    filt_args.add_argument('--ff', '--filt-flags', metavar='INT', type=int, default=1796, dest='filt_flags',
        help='Filter flags: skip reads with mask bits set [%(default)s].')

    sampl_args = parser.add_argument_group('Samples arguments')
    sampl_args.add_argument('-s', '--samples', metavar='STR', nargs='*',
        help='Only use reads with matching samples. You can use multiple regex patterns\n'
            'or exact sample names. Pattern must match the sample name from start to end.')
    sampl_args.add_argument('--join-samples', action='store_true',
        help='Do not split single BAM/CRAM file into multiple columns with different samples.')

    fmt_args = parser.add_argument_group('Format arguments')
    fmt_args.add_argument('-B', '--skip-bq', action='store_true',
        help='Do not print base qualities.')
    fmt_args.add_argument('-n', '--show-names', action='store_true',
        help='Print read names in the header.')
    fmt_args.add_argument('--header', choices=['skip', 'comment', 'plain'],
        metavar='skip|comment|plain', default='plain',
        help='How to write headers. If comment, every header line will start with "#".')
    fmt_args.add_argument('--skip-legend', action='store_true',
        help='Do not write legend.')
    fmt_args.add_argument('-e', '--skip-empty', action='store_true',
        help='Skip lines with zero coverage')
    fmt_args.add_argument('--skip-chrom', action='store_true',
        help='Do not show chromosome name')

    col_args = parser.add_argument_group('Coloring arguments')
    col_args.add_argument('-S', '--scheme', metavar='none|ansi|white|black',
        choices=['none', 'ansi', 'white', 'black'],
        help='Possible color schemes:\n'
            '    none - no colors (default if -o FILE),\n'
            '    ansi - 16 colors,\n'
            '    white - 256 colors with white background,\n'
            '    black - 256 colors with black background (default unless -o FILE).')
    col_args.add_argument('--no-logo', action='store_false', dest='use_logo',
        help='Do not use logo colors for nucleotides.')
    col_args.add_argument('-C', '--no-columns', action='store_true',
        help='Do not highlight columns with a different color.')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    other.add_argument('-V', '--version', action='version', version=__version__,
        help='Show version and exit.')

    args = parser.parse_args()
    try:
        process(args)
    except (BrokenPipeError, KeyboardInterrupt):
        sys.stderr.write('Interrupted\n')
        exit(1)
    except Exception as e:
        sys.stderr.write(str(Text('ERROR:\n', 'colored' in sys.modules).fg('red')))
        raise e


if __name__ == '__main__':
    main()
